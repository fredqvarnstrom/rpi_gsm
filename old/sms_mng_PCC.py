# For Remote Debugging on VS
# import ptvsd
# ptvsd.enable_attach(secret='pi')

import os
import sys
import logging
import glob
import subprocess
import threading
import time

import RPi.GPIO as GPIO
import time
from gsmmodem.modem import GsmModem
from gsmmodem.exceptions import PinRequiredError, IncorrectPinError, TimeoutException, CmsError

cur_dir = os.path.dirname(os.path.realpath(__file__))


class SMSMng:
    # Declare variables here
    # These variables can be used with prefix "self." anywhere, and also can be referred with prefix "self."
    # e.g. self.AlarmPin
    AlarmPin = 21  # input alarm pin
    OverridePin = 16  # input override pin
    LEDPin_Blue = 20  # output Blue LED pin
    LEDPin_White = 12  # output White LED pin

    Phone1 = '5713755499'  # Alarm Phone Number
    Phone2 = '7036491190'  # Drill Phone Number

    AlarmFlag = 0

    DrillCode = 'Drill'
    ArmCode = 'Arm'
    ORCode = 'or'

    SAFE_TIME = 10  # Safe time for pin events

    f = glob.glob(cur_dir + '/*.mp3')  # organize stored mp3(s) for playback

    modem = None
    led_blink = None
    current_mode = 'Arm'  # Initial mode is Arm mode

    event_time = 0

    def __init__(self, port='/dev/ttyS0', baudrate=9600):
        logging.debug('Entering SMS Constructor')
        logging.debug('Current Mode: {}'.format(self.current_mode))
        logging.debug('Drill Mode Phone Number: {}'.format(self.Phone2))
        logging.debug('Drill Code: {} - Arm Code: {} - ORCode: {}'.format(self.DrillCode, self.ArmCode, self.ORCode))

        GPIO.setmode(GPIO.BCM)
        self.GPIOSettings()
        self.turn_sim800_on()

        self.modem = GsmModem(port=port, baudrate=baudrate, smsReceivedCallbackFunc=self.handle_sms)
        self.modem.smsTextMode = False

        self.led_blink = LEDBlink(override_pin=self.OverridePin, led_pin_blue=self.LEDPin_Blue,
                                  led_pin_white=self.LEDPin_White)
        self.event_time = 0
        logging.debug('Exiting SMS Constructor')

    def GPIOSettings(self):
        logging.debug('Entering GPIO Settings')

        try:
            GPIO.cleanup()
            # GPIO pin set up
            GPIO.setwarnings(False)
            GPIO.setup(self.AlarmPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.setup(self.OverridePin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.setup(self.LEDPin_Blue, GPIO.OUT)
            GPIO.setup(self.LEDPin_White, GPIO.OUT)
            GPIO.setup(17, GPIO.OUT)
            GPIO.output(self.LEDPin_Blue, True)
            GPIO.output(self.LEDPin_White, False)
            GPIO.output(17, False)

            # Register callback function of AlarmPin & Override Pin
            GPIO.add_event_detect(self.AlarmPin, GPIO.RISING, callback=self.AlarmPinEvent)
            # GPIO.add_event_detect(self.AlarmPin, GPIO.BOTH, callback=self.AlarmPinEvent)
            GPIO.add_event_detect(self.OverridePin, GPIO.RISING, callback=self.OverridePinEvent)

            logging.debug(
                'Blue LED Pin set as #{}: {}, White LED Pin set as #{}: {}'.format(
                    self.LEDPin_Blue,
                    'High',
                    self.LEDPin_White,
                    'Low'))

            logging.debug(
                'Alarm Pin set as #{}: {}, Override Pin set as # {}: {}'.format(self.AlarmPin,
                                                                                GPIO.input(self.AlarmPin),
                                                                                self.OverridePin,
                                                                                GPIO.input(self.OverridePin)))


        except:
            GPIO.cleanup()
            GPIO.setup(self.AlarmPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.setup(self.OverridePin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.setup(self.LEDPin_Blue, GPIO.OUT)
            GPIO.setup(self.LEDPin_White, GPIO.OUT)
            GPIO.setup(17, GPIO.OUT)  # PWR_KEY is GPIO17
            GPIO.output(self.LEDPin_Blue, True)
            GPIO.output(self.LEDPin_White, False)
            GPIO.output(17, False)
            logging.error('Failed to Initialize GPIO Settings')
        print('GPIO Initialized')
        logging.debug('Exiting GPIO Settings')

    def turn_sim800_on(self):
        logging.debug('Entering turn_sim800_on')
        """
        PWRKEY pin of GSM modem is to turn modem On/Off externally using open collector transistor or switch.
        You can turn on/off the modem alternately by driving the PWRKEY to a low level voltage
        for a short time (2-3 sec) and then release.
        This pin is pulled up to 2.9V in the GSM Modem.
        :return:
        """
        print "Turning SIM800 on..."
        GPIO.output(17, True)
        time.sleep(1)
        GPIO.output(17, False)
        count_down(10)  # Wait for SIM800 to boot

        logging.debug('Exiting turn_sim800_on')

    def connect(self):
        logging.debug('Entering connect')
        time.sleep(10)  # mandatory

        print 'Trying to connect to modem...'

        try:
            self.modem.connect()
            print "Connected successfully..."
            print('Waiting for SMS message...')
        except PinRequiredError:
            sys.stderr.write('Error: SIM card PIN required. Please specify a PIN with the -p argument.\n')
        except IncorrectPinError:
            sys.stderr.write('Error: Incorrect SIM card PIN entered.\n')
        except TimeoutException:
            sys.stderr.write('Error, Cannot connect to SIM800. Please try again later.')

        try:
            # Specify a timeout so that it essentially blocks indefinitely, but still receives CTRL+C interrupt signal
            self.modem.rxThread.join(2 ** 50)
        finally:
            self.modem.close()
            logging.info('Modem Closed')
            print 'Modem Closed'

        logging.debug('Exiting connect')

    def AlarmPinEvent(self, *args):
        """
        This function is called when event is occurred (rising & falling) at the AlarmPin
        :return:
        """
        logging.debug('Entering AlarmPinEvent')
        # Checking for real button Push or if its static by waiting 50ms

        time.sleep(.05)
        if GPIO.input(self.AlarmPin):
            logging.debug('AlarmPinEvent Static')
            logging.debug('Exiting AlarmPinEvent Due to Static')
            print 'AlarmPinEvent Static Received'
            return

        logging.debug('Current Mode: {}'.format(self.current_mode))
        logging.debug('Alarm Pin Value: {}'.format(GPIO.input(self.AlarmPin)))

        print "Event of Alarm Pin is detected.  Setting AlarmFlag..."
        print "Current value: ", GPIO.input(self.AlarmPin)
        if time.time() - self.event_time > self.SAFE_TIME:

            if self.led_blink.stopped():

                # update event time to prevent next event in SAFE_TIME
                self.event_time = time.time()

                # SMS sent to corresponding phone
                alert = 'Facility Magnola is Under Lockdown\nFrom:\nButton 14\nContent:\n'
                alert += 'Alarm Button 14 Activated'
                if self.current_mode == 'Drill':
                    self.send_msg(self.Phone2, alert)
                    # Audio plays
                    player = subprocess.Popen(["omxplayer", self.f[0]],
                                              stdin=subprocess.PIPE)  # ,stdout=subprocess.PIPE,stderr=subprocess.PIPE"
                    logging.info('Playing Alarm Sound')
                else:
                    self.send_msg(self.Phone1, alert)
                    # Audio plays
                    player = subprocess.Popen(["omxplayer", self.f[1]],
                                              stdin=subprocess.PIPE)  # ,stdout=subprocess.PIPE,stderr=subprocess.PIPE"
                    logging.info('Playing Alarm Sound')
                try:
                    self.led_blink.start()

                except RuntimeError:  # Thread is already started
                    logging.error('LEDBlink Thread already running, resuming thread')
                    self.led_blink.resume()

                self.AlarmFlag = 1
                logging.debug('AlarmFlag Set: {}'.format(self.AlarmFlag))

            else:
                print "LED is already blinking..."
                print "SMS message is already sent..."
                logging.debug('LED already running, SMS already Sent')

            logging.debug('Sleeping 5 seconds')
            time.sleep(5)

        else:
            SafeTime = time.time() - self.event_time
            print " #### Cannot perform in SAFE_TIME... ", SafeTime
            logging.debug('Cannot perform in SAFE_TIME: {}'.format(SafeTime))

        logging.debug('Exiting AlarmPinEvent')

    def OverridePinEvent(self, *args):
        """
        This function is called when event is occurred (rising & falling) at the OverridePin
        :return:
        """
        logging.debug('Entering OverridePinEvent')

        # Checking for real button Push or if its static by waiting 50ms
        time.sleep(.05)
        if (GPIO.input(self.OverridePin)):
            logging.debug('OverridePinEvent Static')
            logging.debug('Exiting OverridePinEvent Due to Static')
            print 'OverridePinEvent Static Received'
            return

        logging.debug('Current Mode: {}'.format(self.current_mode))
        logging.debug('Current OverridePin Value: {}'.format(GPIO.input(self.OverridePin)))

        print "Event of Override Pin is detected..."
        print "Current value: ", GPIO.input(self.OverridePin)
        if time.time() - self.event_time > self.SAFE_TIME:

            # update event time to prevent next event in SAFE_TIME
            self.event_time = time.time()

            alert = 'Facility Magnola is Secure\nFrom:\nButton 14\nContent:\n'
            alert += 'Alarm Button 14 Secure'
            if self.current_mode == 'Drill':
                self.send_msg(self.Phone2, alert)
            else:  # Arm mode
                self.send_msg(self.Phone1, alert)

            print("Secure")
            logging.info('Secure')

            self.led_blink.stop()

            self.AlarmFlag = 0
            logging.debug('AlarmFlag Set: {}'.format(self.AlarmFlag))
            logging.debug('Waiting for 5 seconds')
            time.sleep(5)

        else:
            SafeTime = time.time() - self.event_time
            print " #### Cannot perform in SAFE_TIME... ", SafeTime
            logging.debug('Cannot perform in SAFE_TIME: {}'.format(SafeTime))

        logging.debug('Exiting OverridePinEvent')

    def handle_sms(self, sms):
        """
        Callback function when a sms message is received...
        :param sms:
        :return:
        """

        logging.debug('Entering handle_sms')
        print(u'== SMS message received==\nFrom: {0}\nTime: {1}\nMessage:\n{2}'.format(sms.number, sms.time, sms.text))
        logging.debug('SMS Received from {}: {}'.format(sms.number, sms.text))

        print "Current Alarm Flag: ", self.AlarmFlag
        print "Current mode: ", self.current_mode

        logging.debug('Current Alarm Flag: {}'.format(self.AlarmFlag))
        logging.debug('Current Mode: {}'.format(self.current_mode))

        if self.AlarmFlag == 1:
            if sms.text == self.ORCode:  # Alarm Override
                alertMsg = 'Facility Magnola is Secure.\nFrom:\n' + format(sms.number)
                if self.current_mode == 'Drill':
                    self.send_msg(self.Phone2, alertMsg)
                else:  # Arm mode
                    self.send_msg(self.Phone1, alertMsg)

                print("Secure")

                self.led_blink.stop()

                self.AlarmFlag = 0

                return True

            else:  # Ignore all other types of message
                print "Cannot send alarm because AlarmFlag is 1 now..."
                return False

        else:  # When AlarmFlag is 0
            if sms.text == self.ORCode:
                print "Received OR Code though AlarmFlag is 0"
                return True

            elif sms.text == self.DrillCode:  # Set current mode as "Drill" mode and escape
                self.current_mode = 'Drill'
                msg = 'Current Mode Set to: {}'.format(self.current_mode)
                logging.debug(msg)
                print msg
                time.sleep(5)
                return True

            elif sms.text == self.ArmCode:  # Set current mode as "Arm" mode and do escape
                self.current_mode = 'Arm'
                msg = 'Current Mode Set to: {}'.format(self.current_mode)
                logging.debug(msg)
                print msg
                time.sleep(5)
                return True

            else:  # Normal alarm message


                # SMS sent to corresponding phone
                alertMsg = 'Facility Magnola is Under Lockdown.\nFrom: {}\nContent: {}'.format(sms.number, sms.text)
                if self.current_mode == 'Drill':
                    # Audio plays
                    player = subprocess.Popen(["omxplayer", self.f[0]],
                                              stdin=subprocess.PIPE)  # ,stdout=subprocess.PIPE,stderr=subprocess.PIPE"
                    self.send_msg(self.Phone2, alertMsg)
                else:
                    # Audio plays
                    player = subprocess.Popen(["omxplayer", self.f[1]],
                                              stdin=subprocess.PIPE)  # ,stdout=subprocess.PIPE,stderr=subprocess.PIPE"
                    self.send_msg(self.Phone1, alertMsg)

                try:
                    self.led_blink.start()
                except RuntimeError:  # Thread is already started
                    self.led_blink.resume()
                self.AlarmFlag = 1
                return True
        logging.debug('Exiting handle_sms')

    def send_msg(self, phone_number, msg):
        logging.debug('Entering send_msg')

        print ('\nSending SMS to %s, msg: %s' % (phone_number, msg))
        try:
            self.modem.sendSms(phone_number, msg, waitForDeliveryReport=False)
            logging.debug('SMS Sent to {}: {}'.format(phone_number, msg))
        except TimeoutException:
            pass
        except CmsError as e:
            logging.error('Error Sending SMS to {}: {}'.format(phone_number, msg))
            print "Sending Error: ", e

        logging.debug('Exiting send_msg')


def count_down(s):
    for i in range(s, 0, -1):
        print i,
        sys.stdout.flush()
        time.sleep(1)
    print


class LEDBlink(threading.Thread):
    _stop_event = threading.Event()
    OverridePin = 16
    LEDPin_Blue = 20
    LEDPin_White = 12

    def __init__(self, override_pin=16, led_pin_blue=20, led_pin_white=12):
        logging.debug('Entering LEDBlink Constructor')
        threading.Thread.__init__(self)  # constructor of parent class
        self.OverridePin = override_pin
        self.LEDPin_Blue = led_pin_blue
        self.LEDPin_White = led_pin_white
        self._stop_event.set()
        logging.debug('Exiting LEDBlink Constructor')

    def run(self):
        logging.debug('Entering LEDBlink.run')
        self._stop_event.clear()
        while True:
            try:
                if self._stop_event.is_set():
                    GPIO.output(self.LEDPin_Blue, True)
                    GPIO.output(self.LEDPin_White, False)
                    time.sleep(5)
                else:

                    GPIO.output(self.LEDPin_Blue, False)
                    time.sleep(.1)
                    GPIO.output(self.LEDPin_White, True)
                    time.sleep(.1)
                    GPIO.output(self.LEDPin_Blue, True)
                    time.sleep(.1)
                    GPIO.output(self.LEDPin_White, False)
                    time.sleep(.1)
            except:
                GPIO.cleanup()
                time.sleep(.1)
        logging.debug('Exiting LEDBlink.run')

    def stop(self):
        logging.debug('Entering LEDBlink.stop')
        self._stop_event.set()
        logging.debug('Exiting LEDBlink.stop')

    def resume(self):
        logging.debug('Entering LEDBlink.resume')
        self._stop_event.clear()
        logging.debug('Exiting LEDBlink.resume')

    def stopped(self):
        logging.debug('Entering LEDBlink.stopped')
        return self._stop_event.isSet()


if __name__ == '__main__':
    print('Initializing alarm...')

    loggerFormat = '%(asctime)s: %(levelname)-8s %(message)s'
    loggerDateFormat = '%m/%d/%Y %H:%M:%S'
    loggerFileName = '/home/pi/Desktop/LockDown_Py/AlarmLogger.log'

    logging.basicConfig(
        filename=loggerFileName,
        format=loggerFormat,
        datefmt=loggerDateFormat,
        level=logging.DEBUG)

    logging.debug('Starting Alarm...')
    m = SMSMng()

    m.connect()

