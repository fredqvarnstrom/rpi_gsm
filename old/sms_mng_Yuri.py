import os
import sys
import logging
import glob
import subprocess
import threading

import RPi.GPIO as GPIO
import time
from gsmmodem.modem import GsmModem
from gsmmodem.exceptions import PinRequiredError, IncorrectPinError, TimeoutException, CmsError

cur_dir = os.path.dirname(os.path.realpath(__file__))


class SMSMng:
    # Declare variables here
    # These variables can be used with prefix "self." anywhere, and also can be referred with prefix "self."
    # e.g. self.AlarmPin
    AlarmPin = 21  # input alarm pin
    OverridePin = 16  # input override pin
    LEDPin_Blue = 20  # output Blue LED pin
    LEDPin_White = 12  # output White LED pin

    Phone1 = '5713755499'
    Phone2 = '7036491190'

    AlarmFlag = 0

    DrillCode = '12345'
    ArmCode = '54321'
    ORCode = '314159'

    f = glob.glob(cur_dir + '/*.mp3')  # organize stored mp3(s) for playback

    modem = None
    led_blink = None
    current_mode = 'Arm'  # Initial mode is Arm mode

    def __init__(self, port='/dev/ttyS0', baudrate=9600):

        GPIO.setmode(GPIO.BCM)
        self.GPIOSettings()
        self.turn_sim800_on()

        self.modem = GsmModem(port=port, baudrate=baudrate, smsReceivedCallbackFunc=self.handle_sms)
        self.modem.smsTextMode = False

        self.led_blink = LEDBlink(override_pin=self.OverridePin, led_pin_blue=self.LEDPin_Blue,
                                  led_pin_white=self.LEDPin_White)

    def GPIOSettings(self):
        try:
            # GPIO pin set up
            GPIO.setwarnings(False)
            GPIO.setup(self.AlarmPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.setup(self.OverridePin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.setup(self.LEDPin_Blue, GPIO.OUT)
            GPIO.setup(self.LEDPin_White, GPIO.OUT)
            GPIO.setup(17, GPIO.OUT)
            GPIO.output(self.LEDPin_Blue, True)
            GPIO.output(self.LEDPin_White, False)
            GPIO.output(17, False)
        except:
            GPIO.cleanup()
            GPIO.setup(self.AlarmPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.setup(self.OverridePin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.setup(self.LEDPin_Blue, GPIO.OUT)
            GPIO.setup(self.LEDPin_White, GPIO.OUT)
            GPIO.setup(17, GPIO.OUT)  # PWR_KEY is GPIO17
            GPIO.output(self.LEDPin_Blue, True)
            GPIO.output(self.LEDPin_White, False)
            GPIO.output(17, False)
        print('GPIO Initialized')

    def turn_sim800_on(self):
        """
        PWRKEY pin of GSM modem is to turn modem On/Off externally using open collector transistor or switch.
        You can turn on/off the modem alternately by driving the PWRKEY to a low level voltage
        for a short time (2-3 sec) and then release.
        This pin is pulled up to 2.9V in the GSM Modem.
        :return:
        """
        print "Turning SIM800 on..."
        GPIO.output(17, True)
        time.sleep(1)
        GPIO.output(17, False)
        count_down(10)  # Wait for SIM800 to boot

    def connect(self):
        try:
            self.modem.connect()
        except PinRequiredError:
            sys.stderr.write('Error: SIM card PIN required. Please specify a PIN with the -p argument.\n')
        except IncorrectPinError:
            sys.stderr.write('Error: Incorrect SIM card PIN entered.\n')
        except TimeoutException:
            sys.stderr.write('Error, Cannot connect to SIM800. Please try again later.')

        # Register callback function of AlarmPin.
        GPIO.add_event_detect(self.AlarmPin, GPIO.RISING, callback=self.AlarmPinEvent, bouncetime=500)
        # GPIO.add_event_detect(self.AlarmPin, GPIO.BOTH, callback=self.AlarmPinEvent)

        GPIO.add_event_detect(self.OverridePin, GPIO.RISING, callback=self.OverridePinEvent, bouncetime=500)

        print('Waiting for SMS message...')

        try:
            # Specify a timeout so that it essentially blocks indefinitely, but still receives CTRL+C interrupt signal
            self.modem.rxThread.join(2 ** 50)
        finally:
            self.modem.close()

    def AlarmPinEvent(self, *args):
        """
        This function is called when event is occurred (rising & falling) at the AlarmPin
        :return:
        """
        print "Event of Alarm Pin is detected.  Setting AlarmFlag..."
        print "Current value: ", GPIO.input(self.AlarmPin)
        if self.led_blink.stopped():

            # SMS sent to corresponding phone
            alert = 'Facility Magnola is Under Lockdown\nFrom:\nButton 14\nContent:\n'
            alert += 'Alarm Button 14 Activated'
            if self.current_mode == 'Drill':
                self.send_msg(self.Phone2, alert)
                # Audio plays
                player = subprocess.Popen(["omxplayer", self.f[0]],
                                          stdin=subprocess.PIPE)  # ,stdout=subprocess.PIPE,stderr=subprocess.PIPE"
            else:
                self.send_msg(self.Phone1, alert)
                # Audio plays
                player = subprocess.Popen(["omxplayer", self.f[0]],
                                          stdin=subprocess.PIPE)  # ,stdout=subprocess.PIPE,stderr=subprocess.PIPE"
            try:
                self.led_blink.start()

            except RuntimeError:  # Thread is already started
                self.led_blink.resume()

            self.AlarmFlag = 1

        else:
            print "LED is already blinking..."
            print "SMS message is already sent..."

    def OverridePinEvent(self, *args):
        """
        This function is called when event is occurred (rising & falling) at the OverridePin
        :return:
        """
        print "Event of Override Pin is detected..."
        print "Current value: ", GPIO.input(self.OverridePin)
        alert = 'Facility Magnola is Secure\nFrom:\nButton 14\nContent:\n'
        alert += 'Alarm Button 14 Secure'
        if self.current_mode == 'Drill':
            self.send_msg(self.Phone2, alert)
        else:  # Arm mode
            self.send_msg(self.Phone1, alert)

        print("Secure")

        self.led_blink.stop()

        self.AlarmFlag = 0

        time.sleep(20)

    def handle_sms(self, sms):
        """
        Callback function when a sms message is received...
        :param sms:
        :return:
        """
        print(u'== SMS message received==\nFrom: {0}\nTime: {1}\nMessage:\n{2}'.format(sms.number, sms.time, sms.text))

        print "Current Alarm Flag: ", self.AlarmFlag
        print "Current mode: ", self.current_mode

        if self.AlarmFlag == 1:
            if sms.text == self.ORCode:  # Alarm Override
                alert = 'Facility Magnola is Secure\nFrom:\n' + sms.number + '\nContent:\n'
                alert += sms.text
                if self.current_mode == 'Drill':
                    self.send_msg(self.Phone2, alert)
                else:  # Arm mode
                    self.send_msg(self.Phone1, alert)

                print("Secure")

                self.led_blink.stop()

                self.AlarmFlag = 0

                return True

            else:  # Ignore all other types of message
                print "Cannot send alarm because AlarmFlag is 1 now..."
                return False

        else:  # When AlarmFlag is 0
            if sms.text == self.ORCode:
                print "Received OR Code though AlarmFlag is 0"
                return True

            if sms.text == self.DrillCode:  # Set current mode as "Drill" mode and escape
                self.current_mode = 'Drill'
                return True

            if sms.text == self.ArmCode:  # Set current mode as "Arm" mode and do escape
                self.current_mode = 'Arm'
                return True

            else:  # Normal alarm message
                # Audio plays
                player = subprocess.Popen(["omxplayer", self.f[0]],
                                          stdin=subprocess.PIPE)  # ,stdout=subprocess.PIPE,stderr=subprocess.PIPE"

                # SMS sent to corresponding phone
                alert = 'Facility Magnola is Under Lockdown\nFrom:\n' + sms.number + '\nContent:\n'
                alert += sms.text
                if self.current_mode == 'Drill':
                    self.send_msg(self.Phone2, alert)
                else:
                    self.send_msg(self.Phone1, alert)

                try:
                    self.led_blink.start()
                except RuntimeError:  # Thread is already started
                    self.led_blink.resume()

                self.AlarmFlag = 1

                return True

    def send_msg(self, phone_number, msg):
        print ('\nSending SMS to %s, msg: %s' % (phone_number, msg))
        try:
            self.modem.sendSms(phone_number, msg, waitForDeliveryReport=False)
        except TimeoutException:
            pass
        except CmsError as e:
            print "Sending Error: ", e


def count_down(s):
    for i in range(s, 0, -1):
        print i,
        sys.stdout.flush()
        time.sleep(1)
    print


class LEDBlink(threading.Thread):
    _stop_event = threading.Event()
    OverridePin = 16
    LEDPin_Blue = 20
    LEDPin_White = 12

    def __init__(self, override_pin=16, led_pin_blue=20, led_pin_white=12):
        threading.Thread.__init__(self)  # constructor of parent class
        self.OverridePin = override_pin
        self.LEDPin_Blue = led_pin_blue
        self.LEDPin_White = led_pin_white
        self._stop_event.set()

    def run(self):
        self._stop_event.clear()
        while True:
            try:
                if self._stop_event.is_set():
                    GPIO.output(self.LEDPin_Blue, True)
                    GPIO.output(self.LEDPin_White, False)
                    time.sleep(.2)
                else:

                    GPIO.output(self.LEDPin_Blue, False)
                    time.sleep(.1)
                    GPIO.output(self.LEDPin_White, True)
                    time.sleep(.1)
                    GPIO.output(self.LEDPin_Blue, True)
                    time.sleep(.1)
                    GPIO.output(self.LEDPin_White, False)
                    time.sleep(.1)
            except:
                GPIO.cleanup()
                time.sleep(.1)

    def stop(self):
        self._stop_event.set()

    def resume(self):
        self._stop_event.clear()

    def stopped(self):
        return self._stop_event.isSet()


if __name__ == '__main__':
    print('Initializing modem...')
    logging.basicConfig(format='%(asctime)s: %(message)s', datefmt='%m/%d/%Y %H:%M:%S', level=logging.WARNING)
    m = SMSMng()

    m.connect()

