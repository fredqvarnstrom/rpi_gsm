import serial
import os, time

override = '12345'


class SMSReceiver:
    ser = None
    timeout = 5
    baudrate = 9600

    def __init__(self, port='/dev/ttyS0', baudrate=9600, timeout=3):
        self.ser = serial.Serial(port=port, baudrate=baudrate, timeout=timeout)
        self.timeout = timeout
        self.baudrate = baudrate

    def initialize(self):
        print "-- Initializing..."

        self.send_cmd('AT')  # Send 'AT' command to check modem
        rcv = self.read_data()
        if 'OK' in rcv:
            print "Discovering modem: ", rcv
        else:
            print "Failed to discover modem, received val: ", rcv
            return False
        time.sleep(1)

        # print "Current baudrate: ", self.get_baudrate()
        # self.set_baudrate(self.baudrate)

        if not self.check_sim_card():
            return False

        self.send_cmd('ATE0')  # Disable the Echo
        rcv = self.read_data()
        if 'OK' in rcv:
            print "Disable Echo: ", rcv
        else:
            print "Failed to disable Echo, received val: ", rcv
            return False
        time.sleep(1)

        self.send_cmd('AT+CMGF=1')  # Select Message format as Text mode
        rcv = self.read_data()
        if 'OK' in rcv:
            print "Setting Text mode: ", rcv
        else:
            print "Failed to set TEXT mode, received val: ", rcv
            return False
        time.sleep(1)

        self.send_cmd('AT+CNMI=2,1,0,0,0')  # New SMS Message Indications
        rcv = self.read_data()
        if 'OK' in rcv:
            print "Message indication: ", rcv
        else:
            print "Failed to set New SMS Message Indication, received val: ", rcv
            return False
        time.sleep(1)

        return True

    def send_cmd(self, cmd):
        # Transmitting AT Commands to the Modem
        # '\r\n' indicates the Enter key
        cmd += '\r\n'
        self.ser.write(cmd)

    def read_data(self):
        s_time = time.time()
        line = ''
        while True:
            if time.time() - s_time > self.timeout:
                break
            try:
                r = self.ser.read()
                if r == '\n' and r[-1] == '\r':
                    break
                else:
                    line += r
            except ValueError as e:
                print e
                time.sleep(0.1)
            except serial.SerialException as e:
                print e
                time.sleep(0.1)

        return line.strip()

    def check_sim_card(self):
        self.send_cmd('AT+CPIN?')
        rcv = self.read_data()
        if 'CPIN: READY' in rcv:
            print "Found SIM card: ", rcv
            return True
        else:
            print "SIM card not found, received val: ", rcv
            return False

    def set_baudrate(self, baud_rate):
        self.send_cmd('AT+IPR=' + str(baud_rate))
        rcv = self.read_data()
        print rcv

    def get_baudrate(self):
        self.send_cmd('AT+IPR?')
        rcv = self.read_data()
        return rcv

    def send_msg(self, msg, phone_nmber):
        self.send_cmd('')

    def receive_msg(self):
        rcv = self.read_data()
        # Extract the message number shown in between the characters "," and '\r'
        msg = ''
        print "Message received: ", rcv
        try:
            index = rcv.index(",")
            msg_no = rcv[index + 1:].strip()
            print "Message number: ", msg_no
            self.send_cmd('AT+CMGR=' + msg_no)
            msg = self.read_data()

            # delete received message
            self.send_cmd("AT+CMGD=" + msg_no)
            rcv = self.read_data()
            if 'OK' in rcv:
                print "Successfully removed message from inbox."
            else:
                print "Failed to remove message."

        except ValueError as e:
            print "Wrong packet, ", e
        except IndexError as e:
            print "Wrong packet, ", e

        return msg

    def run(self):
        print "Waiting for new message..."
        while True:
            msg = self.receive_msg()
            if len(msg) > 0:  # check if any data received
                print "Total message: ", msg
                try:
                    result = msg.splitlines()[1]
                    print "Message content: ", result
                except IndexError:
                    pass

            time.sleep(0.1)

if __name__ == '__main__':
    # If you need to use different port, baudrate, timeout, just identify it something like:
    #     sms = SMSReceiver(port='/dev/ttyAMA0', baudrate=115200, timeout=2)

    sms = SMSReceiver()
    if sms.initialize():
        sms.run()



