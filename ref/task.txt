Yuri,

You will see that I have hired you again!
Sorry for the delay...so, so much has changed
that we needed to get a bearing on where this project is going but now I believe we know.

So, there are 2 major updates that we need to make with the code.
However, before we get started and before I send you the current code,
I wanted give you a recent history update.

Recent history:
We seem to have overcome any "bouncing" problems, etc. with the current code.
This I believe could only be done in person and was done with the actual demo system in the U.S.
because the current draw (power) issues couldn't really be addressed well remotely without having the
system itself to test (with lights, sounds, etc).
Once we sat down and tested the current code with everything plugged in we were able to address
false alarms/secures and eliminate them.
THAT SAID, I am not really happy with the solution because you can see on the logging console
that false alarms/secures are still coming in on a hardware level and are just being ignored by the software.

That's great, but we want no false alarms/secures...a second engineer has been hired to solve this
problem in hardware, so it is not our concern...we should assume de-bounce will be fixed in hardware and
ignore it from here on out...we have timing requirements that people are concerned about so the less
delays the better.

New stuff:

(1)  We need to re-build the code.  The SMS is working and does not need to be re-built.
However, we have to expand the button inputs dramatically,
so my idea is this: we currently have 12 inputs available on one Pi.
I propose (and has been accepted) using 10 of these to address the buttons and 1 extra button
as a trigger (for a total of 11).
So, one of the GPIO's is used for an initiation event (the eleventh...or first, or whatever) that
the Pi will expect to always be in a high voltage state.
Then, if someone knocks it off the wall power is lost and it will go low, triggering an alarm.
BUT, it will also cause the Pi to poll the state of the remaining 10 pins.
Now, we'll use hardware to keep those 10 pins in the same state for one or two seconds.
The state of each pin will then form a 10-bit address that will be special for each input button.
With DIP switches, each button can have a 10-bit address.
So what the code does is sit and wait for the initiation pin (call it pin 11) to go low,
and then it has maybe a second or so to poll the 10 input pins.
That will give us 2^10 possible button addresses.
i.e., 0010110100 for one button, 1101100110 for a different button.
So, look for the initiation pin, then if it happens determine the address of the button that's pressed.
If no code is found other than the 11th pin going to zero (say, 0000000000) within (maybe) 5 seconds,
a general alarm occurs that indicates a power or system failure.

(2)  We need to add phone call and email functionality to the code.
I know that the GSM card/chip can perform these functions.
Specifically, the system needs to have a pre-determined phone number that is called
(in the U.S. it is "911", but we should be able to call different numbers, too.) on alarm, and a
pre-recorded mp3 file needs to play.
You know AT commands better than me, so we can either loop the mp3 file for (say) 2 minutes
automatically, or (better) not play the mp3 file until the phone call is answered...
I'm assuming the GSM card can determine if the phone has been answered.
Additionally, an email needs to be sent to one or more pre-determined email addresses
(just like the text message).
 Since we're scaling up, I think it makes sense to create SMS and email arrays and send messages to
 each of them respectively.
Maybe a phone number array should be added, too, but I don't think we're going to call more than
one phone number so that would just be a placeholder for future development
(for example, if we call one number and play the message for 30 seconds,
then hang up and call a second number...etc...but that's NOT for now).

I'm probably leaving an idea or two out but I think this should be a little descriptive of
where we're trying to go.  Please let me know if you have any questions.
We've not got much time...I think maybe 2 weeks or so until we start testing.
Installation of the system will begin in six weeks.
Can you tackle it?  Mostly this should be a logic thing...we shouldn't be worrying about
hardware (power) problems...if it works on your system, it should work on mine and hardware
can tackle power issues.

I'll send you the current code tonight.  And thanks Yuri!!

Patrick




Yuri,

This is the final pinout  (attached).
The email is to a colleague and is just an explanation of the pinout.

Patrick

---------- Forwarded message ----------
From: "Patrick Cesarano" <patrick.cesarano@gmail.com>
Date: Oct 7, 2016 11:33 AM
Subject: Pinout
To: "mark lutwyche" <mark_lutwyche@yahoo.com>
Cc:

Mark,

Attached is the pinout we can use with my labels as to whether they refer to the GPIO number
or pin number (they're different) and whether they're inputs or outputs.
There are 12 inputs (10 plus one initiator pin and one "fake" pin) and 3 output pins.
The rest are being used by the GSM card.

I'm a little concerned about the small number of outputs...currently I'm using two in the demo
(one to drive the blue LED relay and one to drive the white LED relay), although only the white LED
relay is used exclusively by the enclosure to make it look sleek so we could easily free that one up.

More importantly, though, is that the enclosure relays are not big enough to allow currents to flow
sufficient to power all of the light/horn's in (say) a mall, so we'll have to have a power
distribution network somewhere in the facility.
However, that also means we could use a power distribution cluster to house other intermediate
circuitry, including (maybe) a second Raspberry to handle completely different functions.

Finally, this is the hardware I've been using for the pinout:

https://www.amazon.com/gp/product/B011CZ2LEY/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1

One end of the ribbon cable connects to the Raspberry, and the other end connects to the pinout
 board that divides the pins into 3.3V and 5V power, and the rest are distributed to the various GPIO's,
 grounds, etc.  The pins are labeled with their GPIO #'s and not the actual pin numbers as is evident.

The Raspberry physical pinout is given here, with pins and GPIO's, etc listed:
https://www.element14.com/community/servlet/JiveServlet/previewBody/73950-102-10-339300/pi3_gpio.png

Patrick
