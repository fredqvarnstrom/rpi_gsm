"""

LOCKDOWN_RPI application v2.

DATE: 10/22/2016

"""
import csv
import traceback
import xml.etree.ElementTree
import os
import sys
import logging
import subprocess
import threading
import serial
import RPi.GPIO as GPIO
import time

import signal

cur_dir = os.path.dirname(os.path.realpath(__file__))

debug = False                # debug mode
b_led = True               # Enable LED or not
b_pin = True               # Enable Pin events or not


class SMSMng:

    FACILITY_NAME = ''                          # Facility name of current RPi

    # Pin numbers
    INPUT_PINS = []                             # 10 input pin numbers
    INIT_PIN = 0                                # init pin number
    PWR_KEY_PIN = 0                             # PWR_KEY Pin number
    OR_PIN = 0                                  # override pin number
    LED_BLUE_PIN = 0                            # LED blue pin number
    LED_WHITE_PIN = 0                           # LED white pin numer

    or_code = ''                                # Override code

    emer_phone = ''                             # Emergency phone number

    # local variables
    location_list = []
    person_list = []
    event_time = 0
    ser = None                                  # UART controller
    conf_file_name = cur_dir + '/config.xml'    # Configuration file path
    AlarmFlag = False                           # Current state is in alarm or not,  1: Alarm, 0: normal
    led_blink = None                            # Instance of LED blinking class
    b_lock = False                              # Indicates whether UART port is free or not.

    SAFE_TIME = 0                               # Safe time for pin events

    alarm_type = ''                             # Alarm type, must be in ['button pushed', 'sms', 'power failure']
    cur_loc = None                              # Current location which is locked down...

    sms_timeout = 60                            # timeout value when sending sms message

    def __init__(self, uart_port='/dev/ttyS0', uart_baudrate=9600, uart_timeout=3):

        if not self.parse_config_params():
            print "Failed to parse configuration parameters, please check again..."
            sys.exit(1)

        self.set_gpio()

        self.ser = serial.Serial(port=uart_port, baudrate=uart_baudrate, timeout=uart_timeout)
        if not self.initialize_gsm():
            print "Failed to initialize, please check GSM module..."
            sys.exit(1)

        self.led_blink = LEDBlink(led_pin_blue=self.LED_BLUE_PIN, led_pin_white=self.LED_WHITE_PIN)

        if b_pin:
            self.register_pin_events()

        if not self.read_loc_personnel():
            print "Failed to location & personnel file, please check..."
            sys.exit(1)
        self.event_time = 0
        logging.debug('Exiting SMS Constructor')

    def parse_config_params(self):
        """
        Read configuration parameters from 'config.xml' and parse
        :return:
        """
        try:
            self.FACILITY_NAME = self.get_param_from_xml('FACILITY_NAME')

            self.emer_phone = self.get_param_from_xml('EMERGENCY_PHONE')

            self.or_code = self.get_param_from_xml('OR_CODE')

            self.PWR_KEY_PIN = int(self.get_param_from_xml('PWR_KEY_PIN'))
            self.INIT_PIN = int(self.get_param_from_xml('INIT_PIN'))
            self.INPUT_PINS = [int(p) for p in self.get_param_from_xml('INPUT_PINS').split(',')]
            self.OR_PIN = int(self.get_param_from_xml('OVERRIDE_PIN'))
            self.LED_BLUE_PIN = int(self.get_param_from_xml('LED_BLUE_PIN'))
            self.LED_WHITE_PIN = int(self.get_param_from_xml('LEB_WHITE_PIN'))

            self.SAFE_TIME = int(self.get_param_from_xml('SAFE_TIME'))
            self.sms_timeout = int(self.get_param_from_xml('SMS_TIMEOUT'))
            return True

        except:
            traceback.print_exc()
            return False

    def set_gpio(self):
        """
        Configure GPIO type, input & output pins
        :return:
        """
        logging.debug('Entering GPIO Settings')
        try:
            # GPIO.cleanup()
            # GPIO pin set up
            GPIO.setwarnings(False)
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(self.LED_BLUE_PIN, GPIO.OUT)
            GPIO.setup(self.LED_WHITE_PIN, GPIO.OUT)
            GPIO.setup(self.PWR_KEY_PIN, GPIO.OUT)
            GPIO.output(self.PWR_KEY_PIN, False)
            GPIO.output(self.LED_BLUE_PIN, True)
            GPIO.output(self.LED_WHITE_PIN, False)

            for pin in self.INPUT_PINS:
                GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.setup(self.INIT_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.setup(self.OR_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

            print('GPIO Initialized')
            logging.debug('Exiting GPIO Settings')

        except:
            traceback.print_exc()
            GPIO.cleanup()
            logging.error('Failed to Initialize GPIO Settings')

    def register_pin_events(self):
        """
        Register callback functions of trigger pins and their trigger edge...
        :return:
        """
        GPIO.add_event_detect(self.INIT_PIN, GPIO.FALLING, callback=self.init_pin_event)

        GPIO.add_event_detect(self.OR_PIN, GPIO.FALLING, callback=self.or_pin_event)

        logging.debug(
            'Blue LED Pin set as #{}: {}, White LED Pin set as #{}: {}'.format(
                self.LED_BLUE_PIN, 'High', self.LED_WHITE_PIN, 'Low'))

        logging.debug(
            'Init Pin set as #{}: {}, Override Pin set as # {}: {}'.format(
                self.INIT_PIN, GPIO.input(self.INIT_PIN), self.OR_PIN, GPIO.input(self.OR_PIN)))

    def turn_gsm_on(self):
        """
        PWRKEY pin of GSM modem is to turn modem On/Off externally using open collector transistor or switch.
        You can turn on/off the modem alternately by driving the PWRKEY to a low level voltage
        for a short time (2-3 sec) and then release.
        This pin is pulled up to 2.9V in the GSM Modem.
        :return:
        """
        logging.debug('Entering turn_sim800_on')
        print " ------ Turning SIM800 on ------"
        GPIO.output(self.PWR_KEY_PIN, True)
        time.sleep(1)
        GPIO.output(self.PWR_KEY_PIN, False)
        count_down(10)  # Wait for SIM800 to boot
        logging.debug('Exiting turn_sim800_on')
        return True

    def initialize_gsm(self):
        """
        Initialize GSM module ( Turn it on, check SIM card, set TEXT mode)
        :return:
        """
        print " ------ Initializing GSM module ------"
        if not debug:
            if not self.turn_gsm_on():
                return False

        logging.debug('Entering connect')
        if not debug:
            count_down(10)  # mandatory

        self.ser.readall()  # popup all remaining data from the serial line

        print 'Trying to connect to modem...'
        # Send 'AT' command to check modem
        self.send_serial_cmd('AT')
        rcv = self.ser.readline()
        if 'AT' in rcv:  # echo is enabled
            print 'Echo is enabled now, disabling...'
            self.ser.readline()
            self.send_serial_cmd('ATE0')
            self.ser.readline()
            print self.ser.readline()
        elif rcv.endswith('\r\n'):
            print 'Echo is already disabled.'
            print 'Discovering modem:  ', self.ser.readline()
        else:
            print 'Failed to discover modem'

        # Check SIM card
        self.send_serial_cmd('AT+CPIN?')
        rcv = self.read_serial_data()
        if 'CPIN: READY' in rcv:
            print "Found SIM card: ", rcv
        else:
            print "SIM card not found or GSM failed to find network, received val: ", rcv
            return False

        # Select Message format as Text mode
        self.send_serial_cmd('AT+CMGF=1')
        rcv = self.read_serial_data()
        if 'OK' in rcv:
            print "Setting Text mode: ", rcv
        else:
            print "Failed to set TEXT mode, received val: ", rcv
            return False
        time.sleep(.5)

        # # New SMS Message Indications
        # self.send_serial_cmd('AT+CNMI=2,1,0,0,0')
        # rcv = self.read_serial_data()
        # if 'OK' in rcv:
        #     print "Message indication: ", rcv
        # else:
        #     print "Failed to set New SMS Message Indication, received val: ", rcv
        #     return False

        # delete all sms messages before starting...
        self.send_serial_cmd('AT+CMGDA="DEL ALL"')
        print 'Deleting all sms messages... ', self.read_serial_data()

        logging.debug('Exiting Initialization...')
        return True

    def init_pin_event(self, *args):
        """
        Callback function when Init Pin is triggered (high -> low)
        :return:
        """
        if self.AlarmFlag:
            print 'Already set as Alarm State...'
            return False

        # Checking for real button Push or if its static by waiting 50ms
        time.sleep(.05)
        if GPIO.input(self.INIT_PIN):
            logging.debug('InitPinEvent Static')
            logging.debug('Exiting InitPinEvent Due to Static')
            print 'InitPinEvent Static Received'
            return False

        print 'Init pin is triggered...'

        time.sleep(.25)
        s_time = time.time()
        while True:
            read_val = ''
            for pin in self.INPUT_PINS:
                buf = '1' if GPIO.input(pin) else '0'
                read_val += buf

            if read_val != '0000000000':
                print "DIP Switch is pressed, value: ", read_val
                self.send_button_alarm(read_val)
                break
            elif time.time() - s_time > 5:
                print "No switch is pressed, generating alarm.."
                self.send_failure_alarm()
                break
            else:
                time.sleep(.1)

    def send_button_alarm(self, pin_state):
        """
        This is when button address is available...
        Compose message and start alarm procedure..
        :param pin_state:
        :return:
        """
        if time.time() - self.event_time > self.SAFE_TIME:
            # update event time to prevent next event in SAFE_TIME
            self.event_time = time.time()

            # Get location
            for loc in self.location_list:
                if loc['address'].strip() == pin_state:
                    self.cur_loc = loc
                    break
            if self.cur_loc is None:
                print "Cannot find location, please check 'location.csv' file..."
                return False

            print "Starting alarm sequence... Location: {}, IP: {}, PIN: {}".format(
                self.cur_loc['location'], self.cur_loc['ip'], self.cur_loc['address'])

            msg = 'Attention, Facility {} is under lockdown.  ' \
                  'The "{}" button has been activated.  ' \
                  'Please view the camera video at "{}" for visual assessment.'.format(self.FACILITY_NAME,
                                                                                       self.cur_loc['location'],
                                                                                       self.cur_loc['ip'])

            logging.debug('Init Pin Value: {}'.format(GPIO.input(self.INIT_PIN)))

            print "Event of Init Pin is detected.  Setting AlarmFlag..."

            self.alarm_type = 'button pushed'
            self.start_alarm(msg)
        else:
            elapsed = int(time.time()) - self.event_time
            print " #### Cannot perform in SAFE_TIME... ", elapsed
            logging.debug('Cannot perform in SAFE_TIME: {}'.format(elapsed))

    def send_failure_alarm(self):
        """
        This is called when no code is found for 5 sec after the INIT pin is triggered.
        :return:
        """
        msg = 'Attention, Facility {} is under lockdown...'.format(self.FACILITY_NAME)
        print "Event of Init Pin is detected, but button address is unavailable..."
        self.alarm_type = 'power failure'
        self.start_alarm(msg)

    def start_alarm(self, msg):
        """
        Start alarm sequence.
        :param msg: Message which will be sent via sms & email
        :return:
        """
        print 'Starting alarm...'
        # TODO: Implement logic of light/horn device

        while self.b_lock:
            time.sleep(.1)
        self.AlarmFlag = True

        if self.led_blink.stopped():
            self.b_lock = True
            # SMS sent to corresponding phones
            for phone_num in [tmp['phone'] for tmp in self.person_list]:
                if len(phone_num.strip()) > 0:
                    self.send_sms(phone_num, msg)

            if not self.AlarmFlag:
                print " ^^^^ System is Secure so quickly....."
                self.b_lock = False
                return False

            # email sent to corresponding addresses
            self.send_mail(self.person_list, msg)

            if not self.AlarmFlag:
                print " ^^^^ System is Secure so quickly....."
                self.b_lock = False
                return False

            if b_led:
                try:
                    self.led_blink.start()
                except RuntimeError:  # Thread is already started
                    logging.error('LEDBlink Thread already running, resuming thread')
                    self.led_blink.resume()

            if self.alarm_type != 'power failure':
                time.sleep(3)
                self.call_phone(self.emer_phone)

            self.b_lock = False
            logging.debug('AlarmFlag Set: {}'.format(self.AlarmFlag))

        else:
            print "LED is already blinking..."
            print "SMS message is already sent..."
            logging.debug('LED already running, SMS already Sent')

    def or_pin_event(self, *args):
        """
        This function is called when event is occurred (rising & falling) at the OverridePin
        :return:
        """
        if not self.AlarmFlag:
            print 'Override pin is triggered though no alarm was enabled...'
            return False

        logging.debug('Entering OverridePinEvent')

        # Checking for real button Push or if its static by waiting 50ms
        time.sleep(.05)
        if GPIO.input(self.OR_PIN):
            logging.debug('OverridePinEvent Static')
            logging.debug('Exiting OverridePinEvent Due to Static')
            print 'OverridePinEvent Static Received'
            return False

        logging.debug('Current OverridePin Value: {}'.format(GPIO.input(self.OR_PIN)))

        print "Event of Override Pin is detected..."
        print "Current value: ", GPIO.input(self.OR_PIN)
        if time.time() - self.event_time > self.SAFE_TIME:
            # update event time to prevent next event in SAFE_TIME
            self.event_time = time.time()
            msg = 'Facility "{}" is Secure from Override button at '.format(self.FACILITY_NAME)
            # TODO: Add detailed message here.
            msg += ''
            self.start_secure(msg)
        else:
            elapsed = int(time.time()) - self.event_time
            print " #### Cannot perform in SAFE_TIME... ", elapsed
            logging.debug('Cannot perform in SAFE_TIME: {}'.format(elapsed))

        logging.debug('Exiting OverridePinEvent')

    def start_secure(self, msg):
        """
        Send secure messages via sms & email
        :param msg:
        :return:
        """
        while self.b_lock:
            time.sleep(.1)
        self.b_lock = True
        # SMS sent to corresponding phones
        for phone_num in [tmp['phone'] for tmp in self.person_list]:
            if len(phone_num.strip()) > 0:
                self.send_sms(phone_num, msg)

        # email sent to corresponding addresses
        self.send_mail(self.person_list, msg)

        self.b_lock = False

        print("Secure")
        logging.info('Secure')

        if b_led:
            self.led_blink.stop()

        self.AlarmFlag = False
        logging.debug('AlarmFlag Set: {}'.format(self.AlarmFlag))
        logging.debug('Waiting for 5 seconds')
        time.sleep(5)

    def call_phone(self, phone_number='911', audio_file=''):
        """
        Call a phone number and play a mp3 file for 90 sec once he answers.
        :param audio_file: Audio file path to play after answer.
        :param phone_number: destination phone number
        :return:
        """
        print ' ------ Starting phone call ------ '
        self.send_serial_cmd('AT+MORING=1')
        rcv = self.read_serial_data()
        if rcv != 'OK':
            print 'Failed to set flag to monitor calling status...'
            return False
        else:
            print 'Enabling call monitor mode:   ', rcv

        self.send_serial_cmd('ATD' + phone_number + ";")
        if self.read_serial_data() != 'OK':
            print 'Failed to start call...'
            return False

        # Wait for answer
        step = 'ready'
        print 'Ringing...'

        s_time = time.time()
        while True:
            recv = self.read_serial_data()
            if recv == '':
                if time.time() - s_time > 60:
                    print 'Failed to call, timeout'
                    return False
                else:
                    time.sleep(.3)
                    continue

            if step == 'ready':
                if 'MO RING' in recv:
                    print 'Received val: ', recv
                    s_time = time.time()
                    step = 'ringing'
                else:
                    print 'Failed to get monitoring data, ', recv
                    return False

            elif step == 'ringing':
                if 'MO CONNECTED' in recv:
                    print 'Connected...'
                    step = 'on_call'
                    s_time = time.time()
                    mp3_file = cur_dir + '/' + self.get_param_from_xml('WARNING_MP3')
                    print 'Playing audio file: ', mp3_file
                    subprocess.Popen(["omxplayer", '--loop', mp3_file], stdin=subprocess.PIPE)

            elif step == 'on_call':
                if 'NO CARRIER' in recv:
                    print "Hung up...."
                    stop_audio()
                    return True
                elif time.time() - s_time > 90:
                    print "Hanging up myself... "
                    self.send_serial_cmd('ATH0')
                    if self.read_serial_data() == 'OK':
                        print 'Successfully ended.'
                        stop_audio()
                        return True

    def send_sms(self, phone_number, msg):
        """
        Send sms message to a phone.
        :param phone_number:
        :param msg:
        :return:
        """
        logging.debug('Entering send_msg')

        print ('\nSending SMS to %s, msg: %s' % (phone_number, msg))

        self.send_serial_cmd('AT+CMGS="' + phone_number + '"')

        rcv = self.read_serial_data()
        if '>' not in rcv:
            print 'Failed to send sms message, no reply, received val: ', rcv
            logging.error('Failed to send sms message, no reply')
            return False

        self.send_serial_cmd(msg + '\x1A')      # Send CTRL+Z to finalize message.

        s_time = time.time()
        while True:
            rcv = self.ser.readline()
            if rcv.startswith('+CMGS:'):
                # Correct response has 3 lines...
                print 'Successfully sent, result: ', rcv, self.ser.readline(), self.ser.readline()
                return True
            elif rcv.startswith('+CMS ERROR'):
                print 'Failed to send sms message, ', rcv
            elif time.time() - s_time > self.sms_timeout:
                print 'Failed to send sms, time out'
                return False
            else:
                time.sleep(.1)
        return True

    def send_mail(self, recipients=[], email_body=''):
        """
        Send mail
        It takes about 60 sec to send email...
        :param email_body: email-body
        :param recipients: recipients in format of dict
                    Sample: [ {'name': 'Jon Smith', 'email': 'John.Smith@yahoo.com'},
                              {'name': 'Andy McArthur', 'email': 'AndyM12@gmail.com'},
                            ]
                    NOTE: Maximum number of recipients is 5
        :return:
        """
        print ' ------ Sending email ------'
        # Check whether this module is already connected to the internet or not.
        self.send_serial_cmd('AT+SAPBR=2,1')
        rcv = self.read_serial_data()
        if rcv.startswith('+SAPBR: 1,3,'):
            print ' *** Starting GPRS mode...'
            # Start by setting up the Email bearer profile identifier.
            self.send_serial_cmd('AT+SAPBR=3,1,"Contype","GPRS"')
            rcv = self.read_serial_data()
            if 'OK' not in rcv:
                print "Failed to switch to GPRS mode...   ", rcv
                return False
            else:
                print 'Switching to GPRS mode:   OK'
            # Set the APN to to "www" since i am using a Vodafone SIM card.
            # It might be different for you, and depends on the network.
            if debug:
                cmd = 'AT+SAPBR=3,1,"APN","cmnet"'
            else:
                cmd = 'AT+SAPBR=3,1,"APN","epc.tmobile.com"'
            self.send_serial_cmd(cmd)
            rcv = self.read_serial_data()
            if rcv != 'OK':
                print "Failed to set APN...   ", rcv
                return False
            else:
                print "Setting APN:   OK"

            if not debug:
                # Set user name of APN
                self.send_serial_cmd('AT+SAPBR=3,1,"USER","guest"')
                if self.read_serial_data() != 'OK':
                    print "Failed to set User name..."
                    return False

                # Set user password of APN
                self.send_serial_cmd('AT+SAPBR=3,1,"PWD","guest"')
                if self.read_serial_data() != 'OK':
                    print "Failed to set password..."
                    return False

            # Enable the GPRS
            self.send_serial_cmd('AT+SAPBR=1,1')
            # Not sure why SIM900 does not reply sometimes...
            # So that we have to wait for it to reply with any message....
            while True:
                rcv = self.read_serial_data()
                if len(rcv.strip()) > 0:
                    break
                else:
                    time.sleep(.1)
            if rcv != 'OK':
                # Check whether it is already connected...
                # Query if the connection is setup properly, if we get back a IP address then we can proceed
                self.send_serial_cmd('AT+SAPBR=2,1')
                resp = self.read_serial_data()
                if resp.startswith('+SAPBR: 1,1,'):
                    print "Checking IP address", resp
                    return False
                else:
                    print "Failed to enable GPRS ...   ", rcv
                    return False
            else:
                print 'Enabling GPRS:   OK'
                # Query if the connection is setup properly, if we get back a IP address then we can proceed
                self.send_serial_cmd('AT+SAPBR=2,1')
                resp = self.read_serial_data()
                if 'OK' not in resp:
                    print "Failed to get IP ...   ", resp
                    return False
                else:
                    print 'Checking IP address: \n', resp

        # Start by setting up the Email bearer profile identifier
        self.send_serial_cmd('AT+EMAILCID=1')
        rcv = self.read_serial_data()
        if rcv != 'OK':
            print "Failed to set email bearer profile identifier...   ", rcv
            return False
        else:
            print 'Setting email bearer profile identifier:   OK'

        # Set the timeout value within which you will enter the email data to be sent
        self.send_serial_cmd('AT+EMAILTO=60')
        rcv = self.read_serial_data()
        if rcv != 'OK':
            print "Failed to set timeout value...   ", rcv
            return False
        else:
            print 'Setting timeout value:   OK'

        # Set the SMTP server address and port number
        smtp_server = self.get_param_from_xml('SMTP_SERVER')
        smtp_port = self.get_param_from_xml('SMTP_PORT')
        self.send_serial_cmd('AT+SMTPSRV="' + smtp_server + '",' + smtp_port)
        rcv = self.read_serial_data()
        if rcv != 'OK':
            print "Failed to set SMTP server and port number...   ", rcv
            return False
        else:
            print 'Setting SMTP server and its port number "{}:{}":   OK'.format(
                smtp_server, smtp_port
            )

        # Set the email authentication information
        auth_email = self.get_param_from_xml('AUTH_EMAIL')
        auth_pwd = self.get_param_from_xml('AUTH_PWD')
        self.send_serial_cmd('AT+SMTPAUTH=1,"' + auth_email + '","' + auth_pwd + '"')
        rcv = self.read_serial_data()
        if rcv != 'OK':
            print "Failed to set email authentication...   ", rcv
            return False
        else:
            print 'Setting email authentication information:   OK'

        # Set the senders email address and Name
        email_sender = self.get_param_from_xml('EMAIL_SENDER')
        self.send_serial_cmd('AT+SMTPFROM="' + email_sender + '","Yuri"')
        rcv = self.read_serial_data()
        if rcv != 'OK':
            print "Failed to set sender's email...   ", rcv
            return False
        else:
            print "Setting sender email:   {}   OK".format(email_sender)

        # Set the To email address and Name
        for i in range(len(recipients)):
            cmd = 'AT+SMTPRCPT=0,{},"{}","{}"'.format(i, recipients[i]['email'], recipients[i]['name'])
            print 'Recipients setting cmd: ', cmd
            self.send_serial_cmd(cmd)
            rcv = self.read_serial_data()
            if rcv != 'OK':
                print "Failed to set destination email address...   ", rcv
                return False

        # Set the Cc email address and Name
        # self.send_serial_cmd('AT+SMTPRCPT=1,0,"ravi@marsinnovations.in","Ravi"')
        # if self.read_serial_data() != 'OK':
        #     print "Failed to set CC email address..."
        #     return False

        # Set the email Subject
        email_subject = self.get_param_from_xml('EMAIL_SUBJECT')
        self.send_serial_cmd('AT+SMTPSUB="' + email_subject + '"')
        rcv = self.read_serial_data()
        if rcv != 'OK':
            print "Failed to set subject...   ", rcv
            return False
        else:
            print 'Setting email subject:  "{}"  OK'.format(email_subject)

        # Time to enter the email content.
        # Enter the below command and you will get a > prompt after which you should type content and press Ctrl+Z
        # when you are done or send 0x1A hex character.
        self.send_serial_cmd('AT+SMTPBODY=' + str(len(email_body)))
        if 'DOWNLOAD' not in self.read_serial_data():
            print 'Failed to start inserting email body.'
            return False
        self.send_serial_cmd(email_body)
        print 'Inputting email body   ', self.read_serial_data()
        print ' >>> Sending email >>>'
        time.sleep(.3)
        # Send the email by entering the below command, if you get  the URC +SMTPSEND: 1 then email is successfully sent
        self.send_serial_cmd('AT+SMTPSEND')
        if self.read_serial_data() != 'OK':
            print 'Failed to set "Send" command...'

        s_time = time.time()
        while True:
            rcv = self.ser.readline()
            if rcv.startswith('+SMTPSEND:'):
                code = int(rcv.splitlines()[0].split(':')[1])
                if code == 1:
                    print 'Successfully sent, result: ', rcv
                    return True
                else:
                    if code == 61:
                        print 'Network error'
                    elif code == 62:
                        print 'DNS resolve error'
                    elif code == 63:
                        print 'SMTP Connection Error'
                    elif code == 64:
                        print 'Timeout of SMTP server'
                    elif code == 65:
                        print 'SMTP server response error'
                    elif code == 66:
                        print 'Not authentication'
                    elif code == 67:
                        print 'Authentication failed. SMTP server user name or password maybe not correct'
                    elif code == 68:
                        print 'Bad recipient'
                    return False
            elif time.time() - s_time > 30:
                print 'Failed to send email, time out'
                return False
            else:
                time.sleep(.1)
        return True

    def send_serial_cmd(self, cmd):
        """
        Transmitting AT Commands to the Modem
        '\r\n' indicates the Enter key
        :param cmd:
        :return:
        """
        cmd += '\r\n'
        self.ser.write(cmd)

    def read_serial_data(self, terminator='OK'):
        """
        Read data from the serial line
        It responses with '\r\n' first when echo is disabled.
        :param terminator:
        :return:
        """
        def read_until(terminat):
            """
            Read data on serial line until terminator appears
            :param terminat:
            :return:
            """
            buf = ''
            while True:
                dd = self.ser.readline()
                if len(dd) == 0:
                    break
                buf += dd
                if str(dd).endswith(terminat + '\r\n'):
                    break
            return buf

        line = self.ser.readline()

        if line == '\r\n':
            line = read_until(terminator)
            if len(line) > 2:
                return str(line)[:-2]
            else:
                return line
        # Popup dummy data and return none
        elif len(line.strip()) == 0:
            return ''
        else:
            print 'Unexpected data: ', line
            print 'dummy: ', self.ser.readline()
            return ''

    def receive_sms(self):
        """
        Receive sms message and parse it.
        :return:
        """
        self.send_serial_cmd('AT+CMGL="ALL"')
        rcv = self.read_serial_data()
        if rcv == 'OK':
            time.sleep(1)
            return None

        result = []
        i = 0
        lines = rcv.splitlines()
        while i < len(lines):
            if lines[i].startswith('+CMGL'):
                s = lines[i].split('"')
                tmp = dict()
                tmp['sender_phone'] = s[3]
                tmp['sender_name'] = s[5]
                tmp['send_date'] = s[7]
                # Read content until meets next sms message or ending indicator
                buf = ''
                index = 1
                while True:
                    try:
                        if lines[i + index] == 'OK' or lines[i + index].startswith('+CMGL'):
                            i += index
                            break
                        else:
                            buf += lines[i + index] + '\n'
                            index += 1
                    except IndexError:
                        break
                tmp['content'] = buf
                result.append(tmp)
            else:
                break

        if len(result) == 0:
            result = None
        else:
            print '*** New message is arrived.'
            for re in result:
                for item in re.items():
                    print item
                print ''
            # delete all sms messages
            self.send_serial_cmd('AT+CMGDA="DEL ALL"')
            print 'Deleting all sms messages... ', self.read_serial_data()
        return result

    def run(self):
        """
        Wait for new sms message and perform corresponding actions
        :return:
        """
        print "Waiting for new sms message..."
        while True:
            if self.b_lock:
                time.sleep(1)
            else:
                rcv_sms = self.receive_sms()
                if rcv_sms is None:
                    continue
                if self.AlarmFlag:
                    or_sms = None
                    for sms in rcv_sms:
                        if self.or_code in sms['content']:
                            or_sms = sms
                            break
                    if or_sms is None:
                        print "Did not Receive OverRide code... "
                        logging.debug('Did not Receive OverRide code... ')
                        continue
                    else:
                        msg = 'Attention, Facility "' + self.FACILITY_NAME + '" is secure.  ' \
                              'The secure message was sent by phone number ' + or_sms['sender_phone'] + \
                              ' at ' + or_sms['send_date'] + '.'

                        self.start_secure(msg)

                else:
                    msg = 'Attention, Facility "' + self.FACILITY_NAME + \
                          '" is under lockdown.  The alarm was initiated by phone number ' + \
                          rcv_sms[0]['sender_phone'] + ' at ' + rcv_sms[0]['send_date'] + \
                          '. Please view the camera video at 192.168.0.101 for visual assessment.'
                    self.alarm_type = 'sms'
                    self.start_alarm(msg)

    def set_param_to_xml(self, tag_name, new_val):
        """
        Set config value to the configuration file with tag name.
        :param tag_name:
        :param new_val:
        :return:
        """
        et = xml.etree.ElementTree.parse(self.conf_file_name)
        for child_of_root in et.getroot():
            if child_of_root.tag == tag_name:
                child_of_root.text = new_val
                et.write(self.conf_file_name)
                return True
        return False

    def get_param_from_xml(self, param):
        """
        Get configuration parameters from the config.xml
        :param param: parameter name
        :return: if not exists, return None
        """
        root = xml.etree.ElementTree.parse(self.conf_file_name).getroot()
        tmp = None
        for child_of_root in root:
            if child_of_root.tag == param:
                tmp = child_of_root.text
                if child_of_root.get('type') == 'file':
                    tmp = cur_dir + '/' + tmp
                break

        return tmp

    def read_loc_personnel(self):
        """
        Read locations and personnel from the csv file and save it to the local variables.
        :return:
        """
        try:
            with open(cur_dir + '/location.csv') as loc_file:
                reader = csv.DictReader(loc_file)
                self.location_list = [row for row in reader]
            if debug:
                f_name = cur_dir + '/personnel_1.csv'
            else:
                f_name = cur_dir + '/personnel.csv'
            with open(f_name) as per_file:
                reader = csv.DictReader(per_file)
                self.person_list = [row for row in reader]
            return True
        except:
            traceback.print_exc()
            return False


def count_down(s):
    for i in range(s, 0, -1):
        print i,
        sys.stdout.flush()
        time.sleep(1)
    print


def stop_audio():
    """
    Stop audio playback - kill omxplayer process in fact.
    :return:
    """
    pid_list = []
    cmd_result = os.popen('ps ax | grep omxplayer | grep -v grep')
    for line in cmd_result:
        fields = line.split()
        pid_list.append(int(fields[0]))

    for pid in pid_list:
        print 'Killing ', pid
        os.kill(pid, signal.SIGKILL)


class LEDBlink(threading.Thread):
    """
    LED blink action class...
    """
    _stop_event = threading.Event()
    LEDPin_Blue = 0
    LEDPin_White = 0

    def __init__(self, led_pin_blue=0, led_pin_white=0):
        logging.debug('Entering LEDBlink Constructor')
        threading.Thread.__init__(self)  # constructor of parent class
        self.LEDPin_Blue = led_pin_blue
        self.LEDPin_White = led_pin_white
        self._stop_event.set()
        logging.debug('Exiting LEDBlink Constructor')

    def run(self):
        logging.debug('Entering LEDBlink.run')
        self._stop_event.clear()
        while True:
            try:
                if self._stop_event.is_set():
                    GPIO.output(self.LEDPin_Blue, True)
                    GPIO.output(self.LEDPin_White, False)
                    time.sleep(5)
                else:
                    GPIO.output(self.LEDPin_Blue, False)
                    time.sleep(.1)
                    GPIO.output(self.LEDPin_White, True)
                    time.sleep(.1)
                    GPIO.output(self.LEDPin_Blue, True)
                    time.sleep(.1)
                    GPIO.output(self.LEDPin_White, False)
                    time.sleep(.1)
            except:
                traceback.print_exc()
                GPIO.cleanup()
                time.sleep(.1)
        logging.debug('Exiting LEDBlink.run')

    def stop(self):
        logging.debug('Entering LEDBlink.stop')
        self._stop_event.set()
        logging.debug('Exiting LEDBlink.stop')

    def resume(self):
        logging.debug('Entering LEDBlink.resume')
        self._stop_event.clear()
        logging.debug('Exiting LEDBlink.resume')

    def stopped(self):
        logging.debug('Entering LEDBlink.stopped')
        return self._stop_event.isSet()


if __name__ == '__main__':
    print('Initializing alarm...')

    loggerFormat = '%(asctime)s: %(levelname)-8s %(message)s'
    loggerDateFormat = '%m/%d/%Y %H:%M:%S'

    loggerFileName = cur_dir + '/AlarmLogger.log'

    logging.basicConfig(
        filename=loggerFileName,
        format=loggerFormat,
        datefmt=loggerDateFormat,
        level=logging.DEBUG)

    logging.debug('Starting Alarm...')
    if debug:
        m = SMSMng('/dev/ttyAMA0')
    else:
        m = SMSMng('/dev/ttyS0')

    # m = SMSMng('/dev/ttyAMA0')

    # rcpt = [{'name': 'Yuri Borys', 'email': 'yuri.borys@programmer.net'},
    #         {'name': 'Yuri Borys', 'email': 'rpiguru@techie.com'},
    #         ]
    # m.send_mail(recipients=rcpt, email_body='Last test...')

    # for p in m.person_list:
    #     if len(p['phone'].strip()) > 0:
    #         m.send_sms(p['phone'], 'Another test...')
    # print m.person_list
    m.run()

