import csv
import traceback

if __name__ == '__main__':
    try:
        with open('location.csv') as loc_file:
            reader = csv.DictReader(loc_file)
            location_list = [row for row in reader]

        with open('personnel_1.csv') as per_file:
            reader = csv.DictReader(per_file)
            person_list = [row for row in reader]
        print location_list
        print " ---- "
        print person_list

    except:
        traceback.print_exc()
