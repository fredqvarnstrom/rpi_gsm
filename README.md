## Preventing Raspberry Pi from using the serial port

The Broadcom UART appears as `/dev/ttyS0` under Linux.

There are several minor things in the way if you want to have dedicated control of the serial port on a Raspberry Pi.

Firstly, the kernel will use the port as controlled by kernel command line contained in `/boot/cmdline.txt`.


The file will look something like this:

    
    dwc_otg.lpm_enable=0 console=ttyAMA0,115200 kgdboc=ttyAMA0,115200 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext4 elevator=deadline rootwait
    
The console keyword outputs messages during boot, and the kgdboc keyword enables kernel debugging.

You will need to remove all references to `ttyAMA0`.

So, for the example above `/boot/cmdline.txt`, should contain:

    dwc_otg.lpm_enable=0 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext4 elevator=deadline rootwait

You must be root to edit this (e.g. use `sudo nano /boot/cmdline.txt`).

Be careful doing this, as a faulty command line can prevent the system booting.

Secondly, after booting, a login prompt appears on the serial port.

This is controlled by the following lines in `/etc/inittab`:

    #Spawn a getty on Raspberry Pi serial line
    T0:23:respawn:/sbin/getty -L ttyAMA0 115200 vt100

You will need to edit this file to comment out the second line, i.e.

    #T0:23:respawn:/sbin/getty -L ttyAMA0 115200 vt100
    
Finally you will need to reboot the Raspberry Pi for the new settings to take effect.

Once this is done, you can use `/dev/ttyS0` like any normal Linux serial port, and you won't get any unwanted traffic confusing the attached devices.

To double-check, use

    cat /proc/cmdline

to show the current kernel command line, and
    
    ps aux | grep ttyS0
    
to search for getty processes using the serial port.

## Install dependencies
    
    sudo pip install python-gsmmodem

## Force audio to play over 3.5mm analog jack.

    sudo nano /etc/rc.local
    
And add this before python execute script we had added before.:

    amixer cset numid=3 1
    
0: Auto, 1: Analog. 2: HDMI


## Enable auto-starting.
    
    sudo apt-get install supervisor
    
    mkdir /home/pi/Desktop/LockDown_Py/log
    sudo nano /etc/supervisor/conf.d/lockdown_rpi.conf
    
And follows to the new file.
    
    [program:lockdown_rpi]
    command = /usr/bin/python /home/pi/Desktop/LockDown_Py/lockdown_rpi.py
    process_name = %(program_name)s
    autorestart=true
    startsecs=10
    startretries=3
    stopsignal=TERM
    stopwaitsecs=10
    directory = /home/pi/Desktop/LockDown_Py
    user = pi
    stdout_logfile = /home/pi/Desktop/LockDown_Py/log/stdout
    stdout_logfile_backups = 5
    stderr_logfile = /home/pi/Desktop/LockDown_Py/log/stderr
    stderr_logfile_maxbytes = 10MB
    stderr_logfile_backups = 5
        
After saving this file, run process with the command below:
    
    sudo supervisorctl reread
    sudo supervisorctl update
    sudo supervisorctl start lockdown_rpi


    



   