import glob
import os

import serial
import time

import subprocess
import RPi.GPIO as GPIO

import signal

import sys

cur_dir = os.path.dirname(os.path.realpath(__file__))


class TestSerial:
    ser = None
    timeout = 5
    baudrate = 9600
    PWR_KEY_PIN = 17  # PWR_KEY Pin number
    f = glob.glob(cur_dir + '/*.mp3')  # organize stored mp3(s) for playback

    def __init__(self, port='/dev/ttyAMA0', baudrate=9600, timeout=3):
        self.ser = serial.Serial(port=port, baudrate=baudrate, timeout=timeout)
        self.timeout = timeout
        self.baudrate = baudrate

        GPIO.setmode(GPIO.BCM)
        GPIO.cleanup()
        GPIO.setup(self.PWR_KEY_PIN, GPIO.OUT)
        GPIO.output(self.PWR_KEY_PIN, False)

    def send_cmd(self, cmd):
        # Transmitting AT Commands to the Modem
        # '\r\n' indicates the Enter key
        cmd += '\r\n'
        self.ser.write(cmd)

    def read_data(self, echo=False):
        """
        Read data from the serial line
        If echo == True, it responses with 'cmd\r\r\n'
        Otherwise, it responses with '\r\n'
        :param echo:
        :return:
        """
        line = self.ser.readline()
        if echo:
            if str(line).endswith('\r\r\n'):

                line = self.ser.readline()

                if len(line) > 2:
                    return str(line)[:-2]
                else:
                    return line
            else:
                self.ser.readline()
                return ''
        else:
            if line == '\r\n':
                line = self.ser.readline()
                if len(line) > 2:
                    return str(line)[:-2]
                else:
                    return line
            else:
                self.ser.readline()
                return ''

    def phone_call(self, phone_number):
        """
        Call a phone number and play mp3 file for 90 sec once he answers
        :param phone_number: destination phone number
        :return:
        """
        self.send_cmd('AT+MORING=1')
        if self.read_data() != 'OK':
            print 'Failed to set flag to monitor calling status...'
            return False

        self.send_cmd('ATD' + phone_number + ";")
        if self.read_data() != 'OK':
            print 'Failed to start call...'
            return False

        # Wait for answer
        step = 'ringing'
        print 'Ringing...'
        s_time = time.time()
        while True:
            recv = self.read_data()
            if step == 'ringing':
                if recv == 'MO CONNECTED':
                    print 'Connected...'
                    step = 'on_call'
                    s_time = time.time()
                    print 'Playing audio file: ', self.f[0]
                    subprocess.Popen(["omxplayer", '--loop', self.f[0]], stdin=subprocess.PIPE)
                elif recv == 'MO RING':
                    if time.time() - s_time > 30:
                        print "No answer, please call afterwards..."
                        return False
                    else:
                        continue
            elif step == 'on_call':
                if recv == 'NO CARRIER':
                    print "Hung up...."
                    stop_audio()
                    return True
                elif time.time() - s_time > 90:
                    print "Hanging up myself... "
                    self.send_cmd('ATH0')
                    if self.read_data() == 'OK':
                        print 'Successfully ended.'
                        stop_audio()
                        return True

    def send_sms(self, phone_number, msg):
        """
        Send sms message to a phone.
        :param phone_number:
        :param msg:
        :return:
        """
        print ('\nSending SMS to %s, msg: %s' % (phone_number, msg))

        self.send_cmd('AT+CMGS="' + phone_number + '"')

        rcv = self.read_data()
        if '>' not in rcv:
            print 'Failed to send sms message, no reply, received val: ', rcv
            return False

        self.send_cmd(msg + '\x1A')      # Send CTRL+Z to finalize message.

        s_time = time.time()
        while True:
            rcv = self.ser.readline()
            if rcv.startswith('+CMGS:'):
                # Correct response has 3 lines...
                print 'Successfully sent, result: ', rcv, self.ser.readline(), self.ser.readline()
                return True
            elif rcv.startswith('+CMS ERROR'):
                print 'Failed to send sms message, ', rcv
            elif time.time() - s_time > 60:
                print 'Failed to send sms, time out'
                return False
            else:
                time.sleep(.1)
        return True

    def turn_sim800_on(self):
        print "Turning SIM800 on..."
        GPIO.output(self.PWR_KEY_PIN, True)
        time.sleep(1)
        GPIO.output(self.PWR_KEY_PIN, False)
        count_down(10)  # Wait for SIM800 to boot

    def receive_sms(self):
        """
        Receive sms message and parse it.
        :return:
        """
        self.send_cmd('AT+CMGL="ALL"')
        rcv = self.read_data()
        if rcv == 'OK':
            return None

        result = []
        i = 0
        lines = rcv.splitlines()
        while i < len(lines):
            if lines[i].startswith('+CMGL'):
                s = lines[i].split('"')
                tmp = dict()
                tmp['sender_phone'] = s[3]
                tmp['sender_name'] = s[5]
                tmp['send_date'] = s[7]
                # Read content until meets next sms message or ending indicator
                buf = ''
                index = 1
                while True:
                    try:
                        if lines[i + index] == 'OK' or lines[i + index].startswith('+CMGL'):
                            i += index
                            break
                        else:
                            buf += lines[i + index] + '\n'
                            index += 1
                    except IndexError:
                        break
                tmp['content'] = buf
                result.append(tmp)
            else:
                break

        # delete all sms messages
        # self.send_serial_cmd('AT+CMGDA="DEL ALL"')
        # print 'Deleting all sms messages... ', self.read_serial_data()
        return result


def count_down(s):
    for i in range(s, 0, -1):
        print i,
        sys.stdout.flush()
        time.sleep(1)
    print


def stop_audio():
    """
    Stop audio playback - kill omxplayer process in fact.
    :return:
    """
    pid_list = []
    cmd_result = os.popen('ps ax | grep omxplayer | grep -v grep')
    for line in cmd_result:
        fields = line.split()
        pid_list.append(int(fields[0]))

    for pid in pid_list:
        print 'Killing ', pid
        os.kill(pid, signal.SIGKILL)

if __name__ == '__main__':

    a = TestSerial('/dev/ttyAMA0')
    res = a.receive_sms()
    for r in res:
        print " ---------- "
        for item in r.items():
            print item

